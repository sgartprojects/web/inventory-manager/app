<?php
  // Session Handling
  @require_once './includes/session.php';
  $sessionHandler = new Session();
  $sessionHandler -> createSession();

  // Set tab title
  if (isset($pageName)) {
    $pageName .= ' - ';
  } else {
    $pageName = '';
  }
?>

<head>
  <title><?= $pageName ?>InventoryManager</title>
  <link rel="icon" href="./assets/web/favicon.ico">
</head>